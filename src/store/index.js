import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    showModal: false
  },
  mutations: {
    setShowModal(state, payload) {
      state.showModal = payload.showModal;
    },
  },
  actions: {
    doShowModal({ commit }, payload) {
      commit('setShowModal', payload);
    },
  },
  modules: {
  }
})
